## Introduction
Ce projet fournit une configuration complète pour automatiser le démarrage de `cmatrix` dans un environnement de terminal, en utilisant `tmux` pour simuler l'effet visuel populaire vu dans le film "Matrix". `cmatrix` s'affiche automatiquement après une période d'inactivité, transformant votre terminal en un écran de style "Matrix".

### Étape 1: Installation de `tmux` et `cmatrix`
Ouvrez un terminal et exécutez la commande suivante pour installer les deux outils :
```bash
sudo apt update
sudo apt install tmux cmatrix
```

### Étape 2: Configuration initiale de `tmux`
Créez ou modifiez le fichier `~/.tmux.conf` pour y ajouter les lignes suivantes :
```bash
# Désactiver la barre de status
# set -g status off
set -g lock-command "cmatrix -C green -o"
set -g lock-after-time 5
```
Cela configurera `tmux` pour démarrer `cmatrix` avec les caractères japonais en vert après 5 secondes d'inactivité.

Si vous souhaiter ralentir le défilement, vous pouvez modifier la commande avec la paramètre `-u` dans le fichier `~/.tmux.conf` :
```bash
set -g lock-command "cmatrix -C green -o -u 8"
```

Après avoir apporté des modifications à votre fichier `~/.tmux.conf`, assurez-vous de recharger la configuration pour que les changement prennent effet. Vous pouvez le faire directement dans tmux en utilisant :
```bash
tmux source-file ~/.tmux.conf
```

### Étape 3: Sécuriser le fichier de configuration
Pour protéger votre fichier de configuration, ajustez les permissions afin que seul le propriétaire puisse le lire ou le modifier :
```bash
chmod 600 ~/.tmux.conf
```

### Étape 4: Démarrage automatique de `tmux`
Pour que `tmux` démarre automatiquement à l'ouverture de votre terminal, ajoutez les lignes suivantes à votre fichier `~/.bashrc` :
```bash
# Démarrage automatique de tmux
if command -v tmux &> /dev/null && [ -z "$TMUX" ]; then
  tmux attach || tmux new-session
fi
```

### Étape 5: Appliquer les modifications
Pour que les changements prennent effet immédiatement, exécutez la commande suivante dans votre terminal :
```bash
source ~/.bashrc
```

### Étape 6: Test de la configuration
Ouvrez un nouveau terminal pour vérifier que `tmux` démarre automatiquement et attendez 5 secondes pour voir si `cmatrix` s'active avec les paramètres désirés.

Pour quitter `cmatrix` entrer la combinaison `control +c`.

### Étape 7: Personnalisation facultative
Si vous souhaitez ajuster d'autres aspects de l'apparence ou du comportement de `tmux` ou de `cmatrix`, vous pouvez modifier le fichier `~/.tmux.conf` avec d'autres options de configuration, comme par exemple changer l'apparence de la barre de statut ou ajouter d'autres raccourcis.

### Résumé
En suivant ces étapes, vous configurez `tmux` pour démarrer `cmatrix` automatiquement dans le style du film "Matrix" après une inactivité de 5 secondes, tout en sécurisant votre configuration.